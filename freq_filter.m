function [Y] = freq_filter(X, fs, filter_name, filter_type, f0 , N)
    L = length(X);

    NFFT = 2^nextpow2(L); % Next power of 2 from length of y
    X_ = fft(X,NFFT);

    f = fs/2*linspace(-1,1,NFFT);
    X_ = fftshift(X_);

    if (filter_name == 'i')
        if (strcmp(filter_type, 'lp'))
            H = (f <= f0 & f >= -f0);
            Y = X_ .* H ;
        elseif (strcmp(filter_type, 'hp'))
            H = (f > f0 & f < -f0);
            Y = X_ .* H ;
        end
    elseif (filter_name == 'b')
        if (strcmp(filter_type, 'lp'))
            H = 1 ./ ( 1 + (f ./ f0).^(2 .* N) );
            Y = X_ .* H ;
        elseif (strcmp(filter_type, 'hp'))
            H = 1 ./ ( 1 + (f0 ./ f).^(2 .* N) );
            Y = X_ .* H ;
        end 
    end
end