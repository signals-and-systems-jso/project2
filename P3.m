clc
clear
close all

%% P3 A
fs = 100;
t1 = -5: 1/fs: 5;
x = @(t) double(not(t));
figure(1);
x_ = x(t1);
plot (t1, x_);

% mohasebeye ferekans khorujiye fft
L = length(x_);
NFFT = 2^nextpow2(L);
f = fs/2*linspace(-1,1,NFFT);

[Y] = freq_filter(x_, fs, 'i', 'lp', 30 , 5);
% [Y] = freq_filter(x_, fs, 'i', 'hp', 30 , 5);
% [Y] = freq_filter(x_, fs, 'b', 'lp', 30 , 5);
% [Y] = freq_filter(x_, fs, 'b', 'hp', 30 , 5);
figure(2)
bar(f,abs(Y)) 

YY = fftshift(Y);
m = ifft(YY, NFFT);

figure(3)
plot(real(m)) 

%% P3 B
fs = 200;
t1 = -5: 1/fs: 5;
x = @(t) (0.4 * sin(pi * t)+0.8 * cos(20 * pi * t)+0.1 * cos(100 * pi * t));
figure(4);
x_ = x(t1);
plot (t1, x_);
L = length(x_);
NFFT = 2^nextpow2(L);
f = fs/2*linspace(-1,1,NFFT);

[Y] = freq_filter(x_, fs, 'i', 'lp', 30 , 5);
% [Y] = freq_filter(x_, fs, 'i', 'hp', 30 , 5);
% [Y] = freq_filter(x_, fs, 'b', 'lp', 30 , 5);
% [Y] = freq_filter(x_, fs, 'b', 'hp', 30 , 5);


figure(5)
plot(f,abs(Y)) 

YY = fftshift(Y);
m = ifft(YY, NFFT);

figure(6)
plot(real(m)) 

%% P3 C

fs = 200;
t1 = -5: 1/fs: 5;
x = @(t) (0.4 * sin(pi * t)+0.8 * cos(20 * pi * t)+0.1 * cos(100 * pi * t));
figure(4);
x_ = x(t1);
plot (t1, x_);
L = length(x_);
NFFT = 2^nextpow2(L);
f = fs/2*linspace(-1,1,NFFT);

[Y] = freq_filter(x_, fs, 'i', 'lp', 5 , 5);
[Y1] = freq_filter(x_, fs, 'i', 'lp', 30 , 5);
[Y2] = freq_filter(x_, fs, 'i', 'lp', 1 , 5);

YY = fftshift(Y);
m = ifft(YY, NFFT , 'symmetric');
figure(7)
plot(m) 
title('0.4*sin(20*pi*t)');
YY = fftshift(Y1-Y);
m = ifft(YY, NFFT , 'symmetric');
figure(8)
plot(m) 
title('0.8*cos(pi*t)');
YY = fftshift(Y2-Y1-Y);
m = ifft(YY, NFFT , 'symmetric');
figure(9)
plot(m) 
title('0.1*cos(100*pi*t)');