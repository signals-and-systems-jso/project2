clc
clear 
close all
%% P2 part A
n = -pi : .1 : pi;
x1 = @(n)(n >= 0 & n <= 4);
b = abs(MFour(x1(n)));
c = angle(MFour(x1(n)));
figure();
h(1) = subplot(1 , 2, 1);
plot(n , b);
title (h(1) , 'abs');
h(2) = subplot(1, 2, 2);
plot(n , c);
title (h(2) , 'angle');
%% P2 part B
x2 = @(n)(x1(n) .* exp (1i .* pi .* n));
b = abs(MFour(x2(n)));
c = angle(MFour(x2(n)));
figure();
h(1) = subplot(1 , 2, 1);
plot(n , b);
title (h(1) , 'abs');
h(2) = subplot(1, 2, 2);
plot(n , c);
title (h(2) , 'angle');
%% P2 part C
f = 100;
t = -5 : 1/f : 5;
x3 = sin(2 .* pi .* t) .* 2 .* (cos( 2.65 .* t)).^2;
b = abs(MFourC(x3 , f));
c = angle(MFourC(x3 , f));
figure();
h(1) = subplot(1 , 2, 1);
plot(t , b);
title (h(1) , 'abs');
h(2) = subplot(1, 2, 2);
plot(t , c);
title (h(2) , 'angle');

