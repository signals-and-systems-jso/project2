clc
clear
close all

%% P5

clear
close all
clc

% t = 0 : .01 : 10; %tafavot mashhood nabood
t = 0 : .001 : 10;
x = sin (10 * pi * t);
y = x .* cos(400*pi*t);
figure
plot(t,x);
title('signal X');
figure
plot(t,y);
title('signal y');

L = length(x);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
X_ = fft(x,NFFT);
% f = 100/2*linspace(-1,1,NFFT);
f = 1000/2*linspace(-1,1,NFFT);
X_ = fftshift(X_);
figure
plot(f,abs(X_));
title('spec X');
L = length(y);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
X_ = fft(y,NFFT);
% f = 100/2*linspace(-1,1,NFFT);
f = 1000/2*linspace(-1,1,NFFT);
X_ = fftshift(X_);
figure
plot(f,abs(X_));
title('spec y');