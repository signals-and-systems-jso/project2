clc
clear 
close all

%% P1 part C
t1 = 0 : .1 : 2;
x = (t1 >= 0 & t1 <= 1) ;
a = abs(fft(x));
figure();
plot (t1, a);

%% P1 part D
t2 = 0 : .01 : 2;
x = (t2 >= 0 & t2 <= 1) ;
b = abs(fftshift(fft(x)));
figure();
plot (t2, b);

%% P1 part E
L = length(x);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
X_ = fft(x,NFFT);
X_ = fftshift(X_);
t3 = linspace( -pi , pi , length(X_));
figure();
bar(t3,abs(X_)) 
title('Single-Sided Amplitude Spectrum of y(t)');
xlabel('Frequency (Hz)');
ylabel('|Y(f)|');

%% P1 part F
t2 = -pi : .01 : pi;
x = (t2 >= 0 & t2 <= 1) ;
b = abs(MFour(x));
figure();
plot (t2, b);

f = 100;
t2 = -.5 : 1/f : .5 ;
x = @(t)sin(t) ;
b = abs(MFourC(x(t2),f));
figure();
t2 = linspace( -.5 , .5 , length(t2));
plot (t2, b);