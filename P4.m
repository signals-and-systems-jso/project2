clc
clear
close all

%% P4
t = 0 : .01 : 10;
x = sin (10 * pi * t);
y = 0.2*cos(100*pi*t);
z = wgn(1 , length(t) , .0001);
figure(1);
plot(t ,x);
figure(2);
m = x + y + z ;
plot(t , m);
figure(3);
L = length(x);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
X_ = fft(x,NFFT);
f = 100/2*linspace(-1,1,NFFT);
X_ = fftshift(X_);
plot(f,abs(X_))
figure(4);
L = length(m);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
X_ = fft(m,NFFT);
f = 100/2*linspace(-1,1,NFFT);
X_ = fftshift(X_);
plot(f,abs(X_))

%% P4 filtering
fs = 100;
t = 0 : 1/fs : 10;
x = sin (10 * pi * t);
y = 0.2*cos(100*pi*t);
z = wgn(1 , length(t) , .0001);
m = x + y + z ;
L = length(m);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
f = 100/2*linspace(-1,1,NFFT);
[Y] = freq_filter(m, fs, 'i', 'lp', 5 , 5);
% [Y2] = freq_filter(m, fs, 'b', 'lp', 100 , 5); %daghigan nemidoonam noise
% ro chejuri filter konam
YY = fftshift(Y);
q = ifft(YY, NFFT , 'symmetric');
figure(5);
plot(q) 

%% P4 data file process aksare code marboot be in bakhsh az stackoverflow jam avari shode :)
load data
figure(6);
plot(data.time , data.data) 

x = data.data;

fs = data.Fs;             %#sampling rate
f0 = 50;                %#notch frequency
fn = fs/2;              %#Nyquist frequency
freqRatio = f0/fn;      %#ratio of notch freq. to Nyquist freq.

notchWidth = 0.1;       %#width of the notch

%Compute zeros
notchZeros = [exp( sqrt(-1)*pi*freqRatio ), exp( -sqrt(-1)*pi*freqRatio )];

%#Compute poles
notchPoles = (1-notchWidth) * notchZeros;

figure(7);
zplane(notchZeros.', notchPoles.');

b = poly( notchZeros ); %# Get moving average filter coefficients
a = poly( notchPoles ); %# Get autoregressive filter coefficients

figure(8);
freqz(b,a,32000,fs)

%#filter signal x
y = filter(b,a,x);
figure(9);
plot ( data.time , y);